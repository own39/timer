import './App.css';
import TimerApp from './component/timer-app'

function App() {
  return (
    <div className="App">
      <TimerApp />
    </div>
  );
}

export default App;
