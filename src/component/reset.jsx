/* eslint-disable no-useless-constructor */
import React from "react";

class Reset extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <button onClick={this.props.resetValue}>Reset</button>
        );
    }

}
export default Reset