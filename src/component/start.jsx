/* eslint-disable no-useless-constructor */
import React from "react";

class Start extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <button onClick={this.props.startTimer}>Start</button>
        );
    }

}
export default Start