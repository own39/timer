/* eslint-disable no-useless-constructor */
import React from "react";

class Input extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <>
                <label>Min</label>
                <input type="number" onChange={e => { this.props.setValueToStateHH(e.target.value) }}></input>

                <label>Sec</label>
                <input type="number" onChange={e => { this.props.setValueToStateMM(e.target.value) }}></input>
            </>
        );
    }

}
export default Input