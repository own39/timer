/* eslint-disable no-useless-constructor */
import React from "react";

class PauseResume extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <button onClick={this.props.setPauseResume}>Pause/Resume</button>
        );
    }

}
export default PauseResume