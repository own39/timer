/* eslint-disable no-useless-constructor */
import React from "react";
import Input from "./input";
import Start from "./start";
import PauseResume from "./pauseResume";
import Reset from "./reset";

class TimerApp extends React.Component {

    state = {
        min: 0,
        sec: 0,
        showMin: 0,
        showSec: 0,
        pauseResume: true
    }

    constructor(props) {
        super(props)
    }

    setValueToStateHH = (m) => {
        this.setState({ min: m })
    }
    setValueToStateMM = (s) => {
        this.setState({ sec: s })
    }

    startTimer = () => {

        this.setState({ showMin: this.state.min })
        this.setState({ showSec: this.state.sec })
        this.callInterval();
    }

    callInterval = () => {
        let value = setInterval(this.counterIncresae, 1000);
    }

    counterIncresae = () => {
        if (this.state.pauseResume) {
            console.log(" Yes")
            let count = this.state.showSec

            //Increase Count
            // if (count >= 60) {
            //     let countMin = this.state.showMin
            //     count = count - 60
            //     countMin++
            //     this.setState({ showMin: countMin })
            //     this.setState({ showSec: count })
            // } else {
            //     count++
            //     this.setState({ showSec: count })
            // }


            if (count < 1) {
                let countMin = this.state.showMin
                count = count + 60
                countMin--
                this.setState({ showSec: count })
                this.setState({ showMin: countMin })
            } else {
                count--
                this.setState({ showSec: count })
            }
        }
    }

    setPauseResume = () => {
        this.setState({ pauseResume: !this.state.pauseResume })
    }

    resetValue = () => {
        this.setState({ showMin: this.state.min })
        this.setState({ showSec: this.state.sec })
    }

    render() {
        let { showSec, showMin } = this.state;
        return (
            <div>
                <div>
                    <Input setValueToStateHH={this.setValueToStateHH} setValueToStateMM={this.setValueToStateMM} />
                    <Start startTimer={this.startTimer} />
                    <PauseResume setPauseResume={this.setPauseResume} />
                    <Reset resetValue={this.resetValue} />
                </div>
                <div>
                    {showMin} : {showSec}
                </div>
            </div>
        );
    }

}
export default TimerApp